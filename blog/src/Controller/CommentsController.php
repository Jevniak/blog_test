<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Comments;
use App\Form\CommentsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController extends AbstractController
{
    /**
     * @Route("/single/{article}/create", name="create_comment")
     */
    public function create(Request $request, Articles $article)
    {
        $comment = new Comments();

        $form = $this->createForm(CommentsType::class,$comment);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
                $comment = $form->getData();
                $comment->setCreatedAt(new \DateTime('now'));
                $comment->setArticleId($article->getId());

                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                $id = $this->generateUrl('single_article',[
                   'article' => $article->getId(),
                ]);
                return $this->redirect($id);
        }

        return $this->render('comments/form.html.twig', [
                'form' => $form->createView(),
                'article' => $article,
        ]);
    }
}
