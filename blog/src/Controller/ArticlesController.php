<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Comments;
use App\Form\ArticlesType;
use App\Form\CommentsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticlesController extends AbstractController
{
    /**
     * @Route("/", name="articles")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $articles = $em->getRepository(Articles::class)->findAll();
        return $this->render('articles/index.html.twig', [
            'controller_name' => 'ArticlesController',
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/single/{article}", name="single_article")
     */
    public function single(Articles $article)
    {
        $em = $this->getDoctrine()->getManager();
        $comments = $em->getRepository(Comments::class)->findBy(array('article_id' => $article->getId()),['id' => 'DESC'],10);
        return $this->render('articles/single.html.twig', [
            'articles' => $article,
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/create", name="create_article")
     */
    public function create(Request $request)
    {
        $article = new Articles();

        $form = $this->createForm(ArticlesType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $article = $form->getData();
            $article->setCreatedAt(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();

            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('articles');
        }
        return $this->render('articles/form.html.twig', [
           'form' => $form->createView(),
            'articles' => null,
        ]);
    }

    /**
     * @Route("/delete/{article}", name="delete_article")
     */
    public function delete(Articles $article)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('articles');
    }

    /**
     * @Route("/update/{article}", name="update_article")
     */
    public function update(Request $request, Articles $article)
    {
        $form = $this->createForm(ArticlesType::class, $article, [
            'action' => $this->generateUrl('update_article',[
                'article' => $article->getId()
            ]),
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $article = $form->getData();
            $article->setUpdatedAt(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('articles');
        }

        return $this->render('articles/form.html.twig', [
            'form' => $form->createView(),
            'articles' => $article,
        ]);
    }
}
